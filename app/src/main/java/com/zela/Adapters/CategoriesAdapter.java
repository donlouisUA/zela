package com.zela.Adapters;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.zela.Ext.ForegroundImageView;
import com.zela.Models.CelebritiesModel;
import com.zela.R;
import com.zela.utils.PaginationAdapterCallback;


import java.util.ArrayList;

import fr.castorflex.android.smoothprogressbar.SmoothProgressBar;

public class CelebritiesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<CelebritiesModel> itemList;
    private Context context;
    private PaginationAdapterCallback mCallback;
    private static final int ITEM = 0;
    private static final int LOADING = 1;

    private boolean isLoadingAdded = false;
    private boolean retryPageLoad = false;
    private String errorMsg;

    public CelebritiesAdapter(Context context, PaginationAdapterCallback mCallback) {
        this.context = context;
        this.mCallback = mCallback;
        itemList = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_list, null);
        RecyclerView.ViewHolder rcv = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        switch (viewType) {
            case ITEM:
                rcv = getviewHolder(parent, inflater);
                break;
            case LOADING:
                View loadingView = inflater.inflate(R.layout.item_progress, parent, false);
                rcv = new LoadingVH(loadingView);
                break;

        }

        return rcv;
    }

    private RecyclerView.ViewHolder getviewHolder(ViewGroup parent, LayoutInflater inflater) {
        RecyclerView.ViewHolder rcv;
        View v1 = inflater.inflate(R.layout.celebrity_view, parent, false);
        rcv = new CelebritiesVH(v1);
        return rcv;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        final CelebritiesModel celebrity=itemList.get(position);

        switch (getItemViewType(position)){
            case ITEM:
                final  CelebritiesVH celebritiesVH=(CelebritiesVH) holder;
                Glide.with(context).load(celebrity.getImage()).into(celebritiesVH.celebrity_photo);
                celebritiesVH.firstName.setText(celebrity.getFirstname());
                celebritiesVH.secondName.setText(celebrity.getLastname());
                break;
            case LOADING:
                LoadingVH loadingVH = (LoadingVH) holder;

                if (retryPageLoad) {
                    loadingVH.mErrorLayout.setVisibility(View.VISIBLE);
                    loadingVH.mProgressBar.setVisibility(View.GONE);

                    loadingVH.mErrorTxt.setText(errorMsg != null ? errorMsg : context.getString(R.string.error_msg_unknown));

                } else {
                    loadingVH.mErrorLayout.setVisibility(View.GONE);
                    loadingVH.mProgressBar.setVisibility(View.VISIBLE);
                }
        }

    }


    @Override
    public int getItemCount() {
        return this.itemList.size();
    }




     /*
   Helpers
   _________________________________________________________________________________________________
    */

    public void add(CelebritiesModel r) {
        itemList.add(r);
        notifyItemInserted(itemList.size() - 1);
    }

    public void addAll(CelebritiesModel[] celebrityResults) {
        for (CelebritiesModel result : celebrityResults) {
            add(result);
        }
    }



    public void remove(CelebritiesModel r) {
        int position = itemList.indexOf(r);
        if (position > -1) {
            itemList.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void clear() {
        isLoadingAdded = false;
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }

    public boolean isEmpty() {
        return getItemCount() == 0;
    }


    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new CelebritiesModel());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = itemList.size() - 1;
        CelebritiesModel result = getItem(position);

        if (result != null) {
            itemList.remove(position);
            notifyItemRemoved(position);
        }
    }

    public CelebritiesModel getItem(int position) {
        return itemList.get(position);
    }

    /**
     * Displays Pagination retry footer view along with appropriate errorMsg
     *
     * @param show
     * @param errorMsg to display if page load fails
     */
    public void showRetry(boolean show, @Nullable String errorMsg) {
        retryPageLoad = show;
        notifyItemChanged(itemList.size() - 1);

        if (errorMsg != null)
            this.errorMsg = errorMsg;
    }


    /*
    View Holders
    ..................................................................................
     */



    protected class LoadingVH extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final SmoothProgressBar mProgressBar;
        private final ImageButton mRetryBtn;
        private final TextView mErrorTxt;
        private final LinearLayout mErrorLayout;

        public LoadingVH(View itemView) {
            super(itemView);

            mProgressBar = (SmoothProgressBar) itemView.findViewById(R.id.loadmore_progress);
            mRetryBtn = (ImageButton) itemView.findViewById(R.id.loadmore_retry);
            mErrorTxt = (TextView) itemView.findViewById(R.id.loadmore_errortxt);
            mErrorLayout = (LinearLayout) itemView.findViewById(R.id.loadmore_errorlayout);
            mRetryBtn.setOnClickListener(this);
            mErrorLayout.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.loadmore_retry:
                case R.id.loadmore_errorlayout:

                    showRetry(false, null);
                    mCallback.retryPageLoad();

                    break;
            }
        }
    }

    protected class CelebritiesVH extends RecyclerView.ViewHolder implements View.OnClickListener {
        private com.zela.Ext.ForegroundImageView celebrity_photo;
        private TextView firstName;
        private TextView secondName;

        public CelebritiesVH(View itemView) {
            super(itemView);
            celebrity_photo = (ForegroundImageView) itemView.findViewById(R.id.celebrity_photo);
            firstName = (TextView) itemView.findViewById(R.id.firstName);
            secondName = (TextView) itemView.findViewById(R.id.secondName);

            int Measuredwidth = 0;
            DisplayMetrics metrics = context.getResources().getDisplayMetrics();
            Measuredwidth = metrics.widthPixels;

            celebrity_photo.getLayoutParams().width = Measuredwidth / 2;
        }

        @Override
        public void onClick(View v) {

        }
    }

    {

    }
}