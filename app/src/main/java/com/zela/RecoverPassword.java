package com.zela;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import butterknife.Bind;
import butterknife.ButterKnife;

public class RecoverPassword extends AppCompatActivity {

    @Bind(R.id.input_verification_code)
    EditText _verification_code;
    @Bind(R.id.input_new_password) EditText _new_password;
    @Bind(R.id.input_reEnterNewPassword) EditText _reEnterNewPassword;
    @Bind(R.id.back_new_password)
    ImageView _back_new_password;
    @Bind(R.id.btn_submit_newPassword)
    Button _submitNewPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recover_password);
        ButterKnife.bind(this);

        _back_new_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
            }
        });

        _submitNewPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitNewPassword();
            }
        });












    }

    private void submitNewPassword() {
        if (!validate()) {
            onSignupFailed();
            return;
        }

        String code = _verification_code.getText().toString();
        String password = _new_password.getText().toString();
        String reEnterPassword = _reEnterNewPassword.getText().toString();

        //Send to server


    }

    public boolean validate() {
        boolean valid = true;

        String code = _verification_code.getText().toString();
        String password = _new_password.getText().toString();
        String reEnterPassword = _reEnterNewPassword.getText().toString();


        if (code.isEmpty() || code.length()!=6) {
            _verification_code.setError("Enter Valid Verification code");
            valid = false;
        } else {
            _verification_code.setError(null);
        }

        if (password.isEmpty() || password.length() < 6 || password.length() > 10) {
            _new_password.setError("between 6 and 10 alphanumeric characters");
            valid = false;
        } else {
            _new_password.setError(null);
        }

        if (reEnterPassword.isEmpty() || reEnterPassword.length() < 4 || reEnterPassword.length() > 10 || !(reEnterPassword.equals(password))) {
            _reEnterNewPassword.setError("Password Do not match");
            valid = false;
        } else {
            _reEnterNewPassword.setError(null);
        }

        return valid;
    }



    public void onSignupFailed() {
        Toast.makeText(getBaseContext(), "Login failed", Toast.LENGTH_LONG).show();

        _submitNewPassword.setEnabled(true);
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }
}
