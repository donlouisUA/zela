package com.zela;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ForgotPassword extends AppCompatActivity {
    @Bind(R.id.back_forgot_password)
    ImageView _back_forgot_password;
    @Bind(R.id.submit_forgot)
    Button _submit_forgot;
    @Bind(R.id.input_email)
    EditText _emailText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        ButterKnife.bind(this);

        _back_forgot_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
            }
        });

        _submit_forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submit_forgot();
            }
        });


    }

    private void submit_forgot() {
        if (!validate()) {
            onSubmitFailed();
            return;
        }

        _submit_forgot.setEnabled(false);

        final ProgressDialog progressDialog = new ProgressDialog(ForgotPassword.this,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Submitting .....");
        progressDialog.show();

//        String address = _addressText.getText().toString();
        String email = _emailText.getText().toString();

        // Submit to server

    }

    public void onSubmitFailed() {
        Toast.makeText(getBaseContext(), "Login failed", Toast.LENGTH_LONG).show();

        _submit_forgot.setEnabled(true);
    }


    public boolean validate() {
        boolean valid = true;

        String email = _emailText.getText().toString();


        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            _emailText.setError("enter a valid email address");
            valid = false;
        } else {
            _emailText.setError(null);
        }


        return valid;
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }
}
