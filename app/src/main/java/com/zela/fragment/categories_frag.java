package com.zela.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zela.Adapters.CategoriesAdapter;
import com.zela.Models.ItemObject;
import com.zela.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ua on 5/31/2017.
 */

public class categories_frag extends Fragment {
    private GridLayoutManager lLayout;
    private View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.fragment_categories, container, false);
        initView();
        return view;
    }

    private void initView() {
        List<ItemObject> rowListItem = getAllItemList();
        lLayout = new GridLayoutManager(getActivity(), 2);

        RecyclerView rView = (RecyclerView)view.findViewById(R.id.recycler_view);
        rView.setHasFixedSize(true);
        rView.setLayoutManager(lLayout);

        CategoriesAdapter rcAdapter = new CategoriesAdapter(getActivity(), rowListItem);
        rView.setAdapter(rcAdapter);
    }

    private List<ItemObject> getAllItemList(){

        List<ItemObject> allItems = new ArrayList<ItemObject>();
        allItems.add(new ItemObject("Cream", R.drawable.ic_cream));
        allItems.add(new ItemObject("Lip Gloss", R.drawable.ic_lip_gloss));
        allItems.add(new ItemObject("Nail Polish", R.drawable.ic_nail_polish));

        return allItems;
    }
}
