package com.zela.fragment;//package com.zela.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zela.R;
import com.zela.mini_fragments.make_up;

import java.util.ArrayList;
import java.util.List;


public class HomeFragment extends Fragment {

    OnHeadlineSelectedListener mCallback;

    public interface OnHeadlineSelectedListener{
        public void setTitle(String title);
    }

    private final int[] inactive_icons = {R.drawable.ic_action_home, R.drawable.ic_action_categories,
            R.drawable.ic_action_looks, R.drawable.ic_action_expert};
    private final int[] active_icons = {R.drawable.ic_action_home_clicked, R.drawable.ic_action_categories_clicked
            , R.drawable.ic_action_looks_active, R.drawable.ic_action_expert_active};
    private String [] titles={"Make-Up","Categories","Looks","Experts"};

    private ViewPager mPager;
    View view;
    MyAdapter adapter;

    TabLayout tabs;

    // TODO: Rename and change types of parameters


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home, container, false);
        initViews();

        // Inflate the layout for this fragment
        return view;
    }

    private void initViews() {
        mPager = (ViewPager) view.findViewById(R.id.viewPager);
        mPager.setPageMargin(getResources().getDisplayMetrics().widthPixels - 7);
        mPager.setClipToPadding(false);
        mPager.setPageMargin(24);
        mPager.setPadding(48, 8, 48, 8);
        mPager.setOffscreenPageLimit(3);
        setUpViewPager(mPager);

        tabs = (TabLayout) view.findViewById(R.id.tabs);
        tabs.setupWithViewPager(mPager);
        setUpTabIcons();
        mCallback.setTitle("Make-Up");
        tabs.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                tabs.getTabAt(tab.getPosition()).setIcon(active_icons[tab.getPosition()]);
                mCallback.setTitle(titles[tab.getPosition()]);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                tabs.getTabAt(tab.getPosition()).setIcon(inactive_icons[tab.getPosition()]);

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                tabs.getTabAt(tab.getPosition()).setIcon(active_icons[tab.getPosition()]);
                getActivity().setTitle(titles[tab.getPosition()]);
            }
        });

    }



    private void setUpTabIcons() {
        tabs.getTabAt(0).setIcon(active_icons[0]);
        tabs.getTabAt(1).setIcon(inactive_icons[1]);
        tabs.getTabAt(2).setIcon(inactive_icons[2]);
        tabs.getTabAt(3).setIcon(inactive_icons[3]);

    }


    private void setUpViewPager(ViewPager mPager) {
        adapter = new MyAdapter(getChildFragmentManager());
        adapter.addFragment(new make_up(), "Make-Up");
        adapter.addFragment(new categories_frag(), "Categories");
        adapter.addFragment(new looks_frag(), "Looks");
        adapter.addFragment(new experts_frag(), "Experts");
        mPager.setAdapter(adapter);

    }

    private static class MyAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragments = new ArrayList<>();
        MyAdapter(FragmentManager fm) {
            super(fm);
        }

        public void addFragment(Fragment fragment, String title) {
            mFragments.add(fragment);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        @Override
        public Fragment getItem(int position) {

            return mFragments.get(position);
//            Bundle args = new Bundle();
//            args.putInt(ChildFragment.POSITION_KEY, position);
//            return ChildFragment.newInstance(args);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return null;
        }

    }

    private static class Adapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragments = new ArrayList<>();
        private final List<String> mFragmentTitles = new ArrayList<>();

        public Adapter(android.support.v4.app.FragmentManager fm) {
            super(fm);
        }

        public void addFragment(Fragment fragment, String title) {
            mFragments.add(fragment);
            mFragmentTitles.add(title);
        }

        @Override
        public Fragment getItem(int position) {

                Bundle args = new Bundle();
                args.putInt(ChildFragment.POSITION_KEY, position);
                return ChildFragment.newInstance(args);


        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return null;
        }
    }


    // TODO: Rename method, update argument and hook method into UI event


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mCallback= (OnHeadlineSelectedListener) context;
        }catch (ClassCastException e){
            throw new ClassCastException(context.toString()
                    + " must implement OnHeadlineSelectedListener");
        }


    }
}


