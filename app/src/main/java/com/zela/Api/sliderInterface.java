package com.zela.Api;

import com.zela.Models.JSONResponse;

import retrofit2.Call;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by ua on 5/6/2017.
 */

public interface categoriesInterface {
    @FormUrlEncoded
    @POST("product_categories")
    Call<JSONResponse> getCategories
            ();


}
