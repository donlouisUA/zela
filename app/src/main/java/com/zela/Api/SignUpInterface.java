package ualabs.wallpaperwizard.api;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;
import ualabs.wallpaperwizard.Models.BaseResponse;

/**
 * Created by ua on 5/6/2017.
 */

public interface ApiCaller {
    @Headers("Authorization:563492ad6f91700001000001d0c083e2722b462045399e570cdbbb88")
    @GET("recent")
    Call<BaseResponse> getRecent
            (@Query("per_page") int per_page,
             @Query("page") int page);

    @Headers("Authorization:563492ad6f91700001000001d0c083e2722b462045399e570cdbbb88")
    @GET("search")
    Call<BaseResponse> getSearchResults(
            @Query("query") String query,
            @Query("per_page") int per_page,
            @Query("page") int page
    );
}
