package com.zela.Api;

import com.zela.Models.JSONResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by ua on 5/6/2017.
 */

public interface SignUpInterface {
    @FormUrlEncoded
    @POST("user_register")
    Call<JSONResponse> signUp
            (@Field("firstname") String firstname,
             @Field("lastname") String lastname,
             @Field("email") String email,
             @Field("phone") String phone,
             @Field("password") String password);


}
