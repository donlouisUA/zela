package com.zela.splash;

import android.content.Intent;

import com.daimajia.androidanimations.library.Techniques;
import com.viksaa.sssplash.lib.cnst.Flags;
import com.viksaa.sssplash.lib.model.ConfigSplash;
import com.zela.MainActivity;
import com.zela.R;
import com.zela.get_started;


/**
 * Created by Don on 4/22/2017.
 */

public class AwesomeSplash extends com.viksaa.sssplash.lib.activity.AwesomeSplash {
    static AwesomeSplash awesomeSplash;

    @Override
    public void initSplash(ConfigSplash configSplash) {
        awesomeSplash = this;
                    /* you don't have to override every property */

        //Customize Circular Reveal
        configSplash.setBackgroundColor(R.color.colorPrimary); //any color you want form colors.xml
        configSplash.setAnimCircularRevealDuration(1000); //int ms
        configSplash.setRevealFlagX(Flags.REVEAL_RIGHT);  //or Flags.REVEAL_LEFT
        configSplash.setRevealFlagY(Flags.REVEAL_BOTTOM); //or Flags.REVEAL_TOP

        //Choose LOGO OR PATH; if you don't provide String value for path it's logo by default

        //Customize Logo
        configSplash.setLogoSplash(R.drawable.logo); //or any other drawable
        configSplash.setAnimLogoSplashDuration(1000); //int ms
        configSplash.setAnimLogoSplashTechnique(Techniques.SlideInUp); //choose one form Techniques (ref: https://github.com/daimajia/AndroidViewAnimations)


        //        //Customize Path
        //        configSplash.setPathSplash(Constants.DROID_LOGO); //set path String
        //        configSplash.setOriginalHeight(400); //in relation to your svg (path) resource
        //        configSplash.setOriginalWidth(400); //in relation to your svg (path) resource
        //        configSplash.setAnimPathStrokeDrawingDuration(3000);
        //        configSplash.setPathSplashStrokeSize(3); //I advise value be <5
        //        configSplash.setPathSplashStrokeColor(R.color.colorAccent); //any color you want form colors.xml
        //        configSplash.setAnimPathFillingDuration(3000);
        //        configSplash.setPathSplashFillColor(R.color.colorSecondary); //path object filling color


        //Customize Title
        configSplash.setTitleSplash("Welcome to Zela");
        configSplash.setTitleTextColor(R.color.colorAccent);
        configSplash.setTitleTextSize(20f); //float value
        configSplash.setAnimTitleDuration(1000);
        configSplash.setAnimTitleTechnique(Techniques.SlideInUp);
        configSplash.setTitleFont("fonts/Roboto-Thin.ttf"); //provide string to your font located in asse
    }

    @Override
    public void animationsFinished() {
        Intent i = new Intent(AwesomeSplash.this, get_started.class);
        startActivity(i);

    }

    public static AwesomeSplash getInstance() {
        return awesomeSplash;
    }
}
