package uamuzisoftwareltd.amualight.Api;


import uamuzisoftwareltd.amualight.Models.CategoriesModel;

public class JSONResponse {
    private CategoriesModel[] payload;
    private String success;

    public JSONResponse(){
    }


    public CategoriesModel[] getPayload() {
        return payload;
    }

    public String getSuccess(){
        return success;
    }


}
