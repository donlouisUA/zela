package com.zela.Models;



public class JSONResponse {
    private PAYLOAD[] payload;
    private String success;
    private String error;

    public JSONResponse(){
    }

    public String getError(){return  error;}

    public void setError(String error){
        this.error=error;
    }

    public String getSuccess(){
        return success;
    }

    public void setSuccess(String success){
        this.success=success;
    }


    public PAYLOAD[] getPayload() {
        return payload;
    }




}
