package com.zela;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.vstechlab.easyfonts.EasyFonts;

public class get_started extends AppCompatActivity {
    TextView zela;
    Button get_started;
    TextView update;
    RelativeLayout bg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_started);
        bg= (RelativeLayout) findViewById(R.id.bg);
//        bg.getBackground().setAlpha(120);


        zela = (TextView) findViewById(R.id.zela);
        zela.setTypeface(EasyFonts.robotoBold(getApplicationContext()));
        get_started = (Button) findViewById(R.id.get_started);
        get_started.setTypeface(EasyFonts.robotoLight(getApplicationContext()));


        update= (TextView) findViewById(R.id.update);
        update.setTypeface(EasyFonts.robotoLight(getApplicationContext()));



        get_started.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(get_started.this, login.class);
                startActivity(intent);
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
            }
        });


    }
}
